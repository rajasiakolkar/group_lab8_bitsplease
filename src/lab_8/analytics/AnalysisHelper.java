/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    
    
    public void userWithMostLikes(){
        
        Map<Integer, Integer> userLikecount = new HashMap<Integer, Integer>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                int likes = 0;
                if (userLikecount.containsKey(user.getId())) {
                    likes = userLikecount.get(user.getId());
                }
                likes += c.getLikes();
                userLikecount.put(user.getId(), likes);
            }
        }
        int max = 0;
        int maxId = 0;
        for(int id : userLikecount.keySet()){
            if(userLikecount.get(id) > max){
                max = userLikecount.get(id);
                maxId = id;
            }
        }
        
        System.out.println("Maximum likes : " + max+ "\nFor "+ users.get(maxId));
    }
    
     public void getFiveMostLikedComment() {

        Map<Integer, Comment> comments = DataStore.getInstance().getComments();

        List<Comment> commentList = new ArrayList<>(comments.values());

        Collections.sort(commentList, new Comparator<Comment>() {

            @Override
            public int compare(Comment o1, Comment o2) {
                return o2.getLikes() - o1.getLikes();
            }
        });
        
        System.out.println("\n5 most liked comments :");
        
        for(int i=0;i<commentList.size() && i<5;i++){
            System.out.println(commentList.get(i));
        }
    }
      public void averageLikesPerComment(){
        //sum of likes by number of comments
        Map<Integer, Integer> userLikecount = new HashMap<Integer, Integer>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        int totalComments=0;
        int likes = 0;
        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                
                if (userLikecount.containsKey(user.getId())) {
                    likes = userLikecount.get(user.getId());
                }
                likes += c.getLikes();
                totalComments++;
            }
        }
        System.out.println("\nAverage Likes per comment ->");
        System.out.println("Total Likes: "+ likes);
        System.out.println("Total comments: " + totalComments);
        System.out.println("Average likes per comments is: " + likes/totalComments);
        
    }
 
    public void postWithMostLikedComment(){
        
        Map<Integer, Integer> postMostLiked = new HashMap<Integer, Integer>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        for (Post post : posts.values()) {
            for (Comment c : post.getComments()) {
                int likes = 0;
                if (postMostLiked.containsKey(post.getPostId())) {
                    likes = postMostLiked.get(post.getPostId());
                }
                likes += c.getLikes();
                postMostLiked.put(post.getPostId(), likes);
            }
        }
        int max = 0;
        ArrayList<Integer> maxIds = new ArrayList<>();
        for(int id : postMostLiked.keySet()){
            if(postMostLiked.get(id) > max){
                max = postMostLiked.get(id);    
            }
        }
        for(int id : postMostLiked.keySet()){
            if(postMostLiked.get(id) == max){
                maxIds.add(id);
            }
        }
        
        System.out.println("\nMaximum Likes in post ->");
        
        for(int count : maxIds){
            for(Post p: posts.values()){
                if(p.getPostId() == count){
                    System.out.println("Post ID which have maximum Likes: " + p.getPostId() + " with total Likes: " + max);
                }
            }
        }
        
    }
    
    public void postWithMostComments(){
        Map<Integer , Integer> postCommentCount =new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        int commentCount =0;
        for(Post post : posts.values()){
            for(Comment c : post.getComments()){
                commentCount=0;
                if(postCommentCount.containsKey(post.getPostId())){
                    commentCount = postCommentCount.get(post.getPostId());
                }
                commentCount ++;
                postCommentCount.put(post.getPostId(),commentCount);
            }
        }
        
        int max = 0;
        ArrayList<Integer> maxIds = new ArrayList<>();
        for(int id : postCommentCount.keySet()){
            if(postCommentCount.get(id) > max){
                max = postCommentCount.get(id);    
            }
        }
       
        for(int id : postCommentCount.keySet()){
            if(postCommentCount.get(id) == max){
                maxIds.add(id);
            }
        }
        
        System.out.println("\nMaximum comments in post ->");
        
        for(int count : maxIds){
            for(Post p: posts.values()){
                if(p.getPostId() == count){
                    System.out.println("Post ID which have maximum comments: " + p.getPostId() + " with total comments: " + max);
                }
            }
        }
        
    }
    
     public void top5InactiveUsersP(){
        Map<Integer , Integer> userPostCount =new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        int postCount =0;
        for(Post post: posts.values()){
            postCount =0;
            if(userPostCount.containsKey(post.getUserId())){
                    postCount = userPostCount.get(post.getUserId());
                }
            postCount++;
            userPostCount.put(post.getUserId(),postCount);
        }
        
        //System.out.println("\nuser to post "+ userPostCount);
        
        List<Integer> sortedPosts = new ArrayList<Integer>(userPostCount.values());
        Collections.sort(sortedPosts, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
            //so as to get ascending list
            return o1 - o2;
            }
        });
        
        //System.out.println("\nSorted "+sortedPosts);
        
        List<Integer> top5List = new ArrayList<>();
        int top5= 5;
        
            for(int i:sortedPosts){
                while(top5 > 0){
                top5List.add(i);
                top5--;
                break;
            }
        }
        //System.out.println("\ntop5List: "+ top5List);
        
        int num = 1;
        List<Integer> added = new ArrayList<Integer>();
        
        System.out.println("\nTop 5 Inactive users based on Posts ->");
        for(int i: top5List ){
                
                for(int id : userPostCount.keySet()){
                    if(userPostCount.get(id) == i && !added.contains(id)){
                         added.add(id);
                         System.out.println(num + ") "+"UserID: "+ id + " Total Posts: "+ i);
                         num++;
                         break;
                    }
                    
                }
        }
        
    }
     
     public void top5InactiveUsersC(){
        
        Map<Integer , Integer> userCommentCount =new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        int commentCount =0;
        for(Post post : posts.values()){
            for(Comment c : post.getComments()){
                commentCount=0;
                if(userCommentCount.containsKey(post.getUserId())){
                    commentCount = userCommentCount.get(post.getUserId());
                }
                commentCount ++;
                userCommentCount.put(post.getUserId(),commentCount);
            }
        }
        //System.out.println("\nunsorted Map " + postCommentCount);
        
        List<Integer> sortedComments = new ArrayList<Integer>(userCommentCount.values());
        Collections.sort(sortedComments, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
            //so as to get ascending list
            return o1 - o2;
            }
        });
        
        //System.out.println("\nSorted "+sortedComments);
        
        List<Integer> top5List = new ArrayList<>();
        int top5= 5;
        
            for(int i:sortedComments){
                while(top5 > 0){
                top5List.add(i);
                top5--;
                break;
            }
        }
        //System.out.println("\ntop5List: "+ top5List);
        
        int num = 1;
        List<Integer> added = new ArrayList<Integer>();
        
        System.out.println("\nTop 5 Inactive users based on Comments using Posting-User-Id->");
            for(int i: top5List ){
                
                for(int id : userCommentCount.keySet()){
                    if(userCommentCount.get(id) == i && !added.contains(id)){
                         added.add(id);
                         System.out.println(num + ") "+"UserID: "+ id + " Total comments: "+ i);
                         num++;
                         break;
                    }   
                }      
        }
    }
    
    public void top5InactiveUsersC2(){
        
        Map<Integer , Integer> userCommentCount =new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        int commentCount =0;
        for(User user : users.values()){
            for(Comment c : user.getComments()){
                commentCount=0;
                if(userCommentCount.containsKey(user.getId())){
                    commentCount = userCommentCount.get(user.getId());
                }
                commentCount ++;
                userCommentCount.put(user.getId(),commentCount);
            }
        }
        //System.out.println("\nunsorted Map " + userCommentCount);
        
        List<Integer> sortedComments = new ArrayList<Integer>(userCommentCount.values());
        Collections.sort(sortedComments, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
            //so as to get ascending list
            return o1 - o2;
            }
        });
        
        //System.out.println("\nSorted "+sortedComments);
        
        List<Integer> top5List = new ArrayList<>();
        int top5= 5;
        
            for(int i:sortedComments){
                while(top5 > 0){
                top5List.add(i);
                top5--;
                break;
            }
        }
        //System.out.println("\ntop5List: "+ top5List);
        
        int num = 1;
        List<Integer> added = new ArrayList<Integer>();
        
        System.out.println("\nTop 5 Inactive users based on Comments using Commenting-User-Id->");
            for(int i: top5List ){
                
                for(int id : userCommentCount.keySet()){
                    if(userCommentCount.get(id) == i && !added.contains(id)){
                         added.add(id);
                         System.out.println(num + ") "+"UserID: "+ id + " Total comments: "+ i);
                         num++;
                         break;
                    }   
                }      
        }
    }
    
    public void top5InactiveUsersO(){
              
        //USER TO COMMENT COUNT
        Map<Integer , Integer> userCommentCount =new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        int commentCount =0;
        for(Post post : posts.values()){
            for(Comment c : post.getComments()){
                commentCount=0;
                if(userCommentCount.containsKey(post.getUserId())){
                    commentCount = userCommentCount.get(post.getUserId());
                }
                commentCount ++;
                userCommentCount.put(post.getUserId(),commentCount);
            }
        }
        //System.out.println("\nuser to comment " + userCommentCount);
       
        //USER TO POST COUNT
        Map<Integer , Integer> userPostCount =new HashMap<>();
        int postCount =0;
        for(Post post: posts.values()){
            postCount =0;
            if(userPostCount.containsKey(post.getUserId())){
                    postCount = userPostCount.get(post.getUserId());
                }
            postCount++;
            userPostCount.put(post.getUserId(),postCount);
        }
        
        //System.out.println("\nuser to post "+ userPostCount);

        //INACTIVE USERS = POSTS+COMMENTS     
        Map<Integer,Integer> inactiveUsers = new HashMap<>();      
          for(int i : userPostCount.keySet()){
              for(int j : userCommentCount.keySet())
                    if(i == j){
                        inactiveUsers.put(i,userPostCount.get(i)+userCommentCount.get(j));
                    }
                }
        //System.out.println("\n" + inactiveUsers);
        
        List<Integer> sortedInactiveUsers = new ArrayList<Integer>(inactiveUsers.values());
        Collections.sort(sortedInactiveUsers, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
            //so as to get ascending list
            return o1 - o2;
            }
        });
        
        //System.out.println("\nSorted "+sortedInactiveUsers);
        
        List<Integer> top5List = new ArrayList<>();
        int top5= 5;
        
            for(int i:sortedInactiveUsers){
                while(top5 > 0){
                top5List.add(i);
                top5--;
                break;
            }
        }
        //System.out.println("\ntop5List: "+ top5List);
        int num = 1;
        List<Integer> added = new ArrayList<Integer>();
        System.out.println("\nTop 5 Inactive users Overall");
            for(int i: top5List ){
                
                for(int id : inactiveUsers.keySet()){
                    if(inactiveUsers.get(id) == i && !added.contains(id)){
                         added.add(id);
                         System.out.println(num + ") "+"UserID: "+ id );
                         num++;
                         break;
                    }   
                }      
        }
        
    }
    
    public void top5ProactiveUserO(){
         Map<Integer , Integer> userCommentCount =new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        int commentCount =0;
        for(Post post : posts.values()){
            for(Comment c : post.getComments()){
                commentCount=0;
                if(userCommentCount.containsKey(post.getUserId())){
                    commentCount = userCommentCount.get(post.getUserId());
                }
                commentCount ++;
                userCommentCount.put(post.getUserId(),commentCount);
            }
        }
        //System.out.println("\nuser to comment " + userCommentCount);
       
        //USER TO POST COUNT
        Map<Integer , Integer> userPostCount =new HashMap<>();
        int postCount =0;
        for(Post post: posts.values()){
            postCount =0;
            if(userPostCount.containsKey(post.getUserId())){
                    postCount = userPostCount.get(post.getUserId());
                }
            postCount++;
            userPostCount.put(post.getUserId(),postCount);
        }
        
        //System.out.println("\nuser to post "+ userPostCount);

        //INACTIVE USERS = POSTS+COMMENTS     
        Map<Integer,Integer> inactiveUsers = new HashMap<>();      
          for(int i : userPostCount.keySet()){
              for(int j : userCommentCount.keySet())
                    if(i == j){
                        inactiveUsers.put(i,userPostCount.get(i)+userCommentCount.get(j));
                    }
                }
        //System.out.println("\n" + inactiveUsers);
        
        List<Integer> sortedInactiveUsers = new ArrayList<Integer>(inactiveUsers.values());
        Collections.sort(sortedInactiveUsers, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
            //so as to get ascending list
            return o2 - o1;
            }
        });
        
        //System.out.println("\nSorted "+sortedInactiveUsers);
        
        List<Integer> top5List = new ArrayList<>();
        int top5= 5;
        
            for(int i:sortedInactiveUsers){
                while(top5 > 0){
                top5List.add(i);
                top5--;
                break;
            }
        }
        //System.out.println("\ntop5List: "+ top5List);
        int num = 1;
        List<Integer> added = new ArrayList<Integer>();
        System.out.println("\nTop 5 Proactive users Overall");
            for(int i: top5List ){
                
                for(int id : inactiveUsers.keySet()){
                    if(inactiveUsers.get(id) == i && !added.contains(id)){
                         added.add(id);
                         System.out.println(num + ") "+"UserID: "+ id );
                         num++;
                         break;
                    }   
                }      
        }
        
    }

    
}
